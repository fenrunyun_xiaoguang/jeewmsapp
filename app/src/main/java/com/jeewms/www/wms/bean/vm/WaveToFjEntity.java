package com.jeewms.www.wms.bean.vm;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;


/**   
 * @Title: Entity
 * @Description: wave_to_fj
 * @author onlineGenerator
 * @date 2019-12-11 11:32:18
 * @version V1.0   
 *
 */

public class WaveToFjEntity implements java.io.Serializable {
	/**主键*/
	private String id;
	/**创建人登录名称*/
 	private String createBy;
	/**创建人名称*/
 	private String createName;
	/**货主*/
 	private String cusCode;
	/**客户名称*/
 	private String cusName;
	/**到货通知单*/
 	private String omNoticeId;
	/**到货通知行项目*/
 	private String iomNoticeItem;
	/**商品编码*/
 	private String goodsId;
	/**商品名称*/
 	private String goodsName;
	/**仓位*/
 	private String binId;
	/**托盘*/
 	private String tinId;
	/**生产日期*/
 	private String proData;
	/**baseGoodscount*/
 	private Double baseGoodscount;
	/**基本单位*/
 	private String baseUnit;
	/**waveId*/
 	private String waveId;
	/**firstRq*/
 	private String firstRq;
	/**secondRq*/
 	private String secondRq;
	/**by1*/
 	private String by1;
	/**by2*/
 	private String by2;
	/**by3*/
 	private String by3;
	/**by4*/
 	private String by4;
	/**by5*/
 	private String by5;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */

	public String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人登录名称
	 */
 	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人登录名称
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人名称
	 */
 	public String getCreateName(){
		return this.createName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人名称
	 */
	public void setCreateName(String createName){
		this.createName = createName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  货主
	 */
 	public String getCusCode(){
		return this.cusCode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  货主
	 */
	public void setCusCode(String cusCode){
		this.cusCode = cusCode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户名称
	 */
 	public String getCusName(){
		return this.cusName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户名称
	 */
	public void setCusName(String cusName){
		this.cusName = cusName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  到货通知单
	 */
 	public String getOmNoticeId(){
		return this.omNoticeId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  到货通知单
	 */
	public void setOmNoticeId(String omNoticeId){
		this.omNoticeId = omNoticeId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  到货通知行项目
	 */
 	public String getIomNoticeItem(){
		return this.iomNoticeItem;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  到货通知行项目
	 */
	public void setIomNoticeItem(String iomNoticeItem){
		this.iomNoticeItem = iomNoticeItem;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品编码
	 */
 	public String getGoodsId(){
		return this.goodsId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品编码
	 */
	public void setGoodsId(String goodsId){
		this.goodsId = goodsId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品名称
	 */
 	public String getGoodsName(){
		return this.goodsName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品名称
	 */
	public void setGoodsName(String goodsName){
		this.goodsName = goodsName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓位
	 */
 	public String getBinId(){
		return this.binId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓位
	 */
	public void setBinId(String binId){
		this.binId = binId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  托盘
	 */
 	public String getTinId(){
		return this.tinId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  托盘
	 */
	public void setTinId(String tinId){
		this.tinId = tinId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  生产日期
	 */
 	public String getProData(){
		return this.proData;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  生产日期
	 */
	public void setProData(String proData){
		this.proData = proData;
	}
	/**
	 *方法: 取得java.lang.Double
	 *@return: java.lang.Double  baseGoodscount
	 */
 	public Double getBaseGoodscount(){
		return this.baseGoodscount;
	}

	/**
	 *方法: 设置java.lang.Double
	 *@param: java.lang.Double  baseGoodscount
	 */
	public void setBaseGoodscount(Double baseGoodscount){
		this.baseGoodscount = baseGoodscount;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  基本单位
	 */
 	public String getBaseUnit(){
		return this.baseUnit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  基本单位
	 */
	public void setBaseUnit(String baseUnit){
		this.baseUnit = baseUnit;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  waveId
	 */
 	public String getWaveId(){
		return this.waveId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  waveId
	 */
	public void setWaveId(String waveId){
		this.waveId = waveId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  firstRq
	 */
 	public String getFirstRq(){
		return this.firstRq;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  firstRq
	 */
	public void setFirstRq(String firstRq){
		this.firstRq = firstRq;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  secondRq
	 */
 	public String getSecondRq(){
		return this.secondRq;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  secondRq
	 */
	public void setSecondRq(String secondRq){
		this.secondRq = secondRq;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  by1
	 */
 	public String getBy1(){
		return this.by1;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  by1
	 */
	public void setBy1(String by1){
		this.by1 = by1;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  by2
	 */
 	public String getBy2(){
		return this.by2;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  by2
	 */
	public void setBy2(String by2){
		this.by2 = by2;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  by3
	 */
 	public String getBy3(){
		return this.by3;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  by3
	 */
	public void setBy3(String by3){
		this.by3 = by3;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  by4
	 */
 	public String getBy4(){
		return this.by4;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  by4
	 */
	public void setBy4(String by4){
		this.by4 = by4;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  by5
	 */
 	public String getBy5(){
		return this.by5;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  by5
	 */
	public void setBy5(String by5){
		this.by5 = by5;
	}
}
