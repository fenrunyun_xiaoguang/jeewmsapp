package com.jeewms.www.wms.ui.itemview;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.jeewms.www.wms.R;
import com.jeewms.www.wms.bean.vm.PickingSaveVm;
import com.jeewms.www.wms.bean.vm.WaveToDownEntity;
import com.jeewms.www.wms.constance.Constance;
import com.jeewms.www.wms.util.CheckUtil;
import com.jeewms.www.wms.util.GsonUtils;
import com.jeewms.www.wms.util.LoadingUtil;
import com.jeewms.www.wms.util.SharedPreferencesUtil;
import com.jeewms.www.wms.util.StringUtil;
import com.jeewms.www.wms.util.ToastUtil;
import com.jeewms.www.wms.util.UUIDUtil;
import com.jeewms.www.wms.volley.HTTPUtils;
import com.jeewms.www.wms.volley.VolleyListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 13799 on 2018/6/3.
 */

public class WavetodownItemView {

    View mView;
    Context mContext;
    ViewHolder holder;
    PickingDetailListent mListent;
    Map<Integer, String> mMapContent;
    String perStr;
    String perStrbin;

    public void setListent(PickingDetailListent listent) {
        mListent = listent;
    }

    public WavetodownItemView(Context act) {
        mContext = act;
        mMapContent = new HashMap<Integer, String>();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mView = inflater.inflate(R.layout.item_wavetodowndetail, null);
        holder=new ViewHolder(mView);
    }

    public View getView() {
        return mView;
    }

    public void bindView(final WaveToDownEntity vm, final int position) {
        final int mPosition=position;

        holder.tvBaseGoodscount.setText(vm.getBaseGoodscount()+" ");
        holder.tvShlDanWei.setText(vm.getBaseUnit());

        holder.tvOmNoticeId.setText(vm.getWaveId());
        holder.tvBinId.setText(vm.getBinId());
        holder.tvShpMingCheng.setText(vm.getGoodsName());
        holder.tvTinId.setText(vm.getTinId());
        holder.tvGoodsProData.setText(vm.getProData());
        holder.tvOmBeiZhu.setText(vm.getOmBeiZhu());
        holder.tvZhongWenQch.setText(vm.getCusName());
        holder.tvTinId3.setText(vm.getFirstRq());

         //界面edittext输入丢失问题
        holder.tvTinId3.setTag(position);

        holder.tvTinId3.setTag(position);
//        holder.tvTinId3.setText("");   //储位必须扫描

        holder.tvTinId3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!StringUtil.isEmpty(perStr)){
                    if(!b&&!perStr.equals(holder.tvTinId3.getText().toString())) {
                        perStr=holder.tvTinId3.getText().toString();
                        mListent.setTinId2(position,holder.tvTinId3.getText().toString());
                    }
                }

            }
        });
        holder.tvTinId3.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(CheckUtil.checkText(mContext,holder.tvTinId3,"请输入"))
                    save(position,"","",holder.tvTinId3.getText().toString(),vm);
                return false;
            }
        });

        holder.tvTinId3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!StringUtil.isEmpty(perStrbin)) {
                    if (!b && !perStrbin.equals(holder.tvTinId3.getText().toString())) {
                        perStrbin = holder.tvTinId3.getText().toString();
                        mListent.setBinId2(position, holder.tvTinId3.getText().toString());
                    }
                }
            }
        });
        holder.tvTinId3.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(CheckUtil.checkText(mContext,holder.tvTinId3,"请输入"))
                    save(position,"","",holder.tvTinId3.getText().toString(),vm);
                return false;
            }
        });


        holder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListent!=null){
                    if(CheckUtil.checkText(mContext,holder.tvTinId3,"请输入"))
                    save(position,"","",holder.tvTinId3.getText().toString(),vm);
                }
            }
        });
    }
    private void save(final int position, String tinId2, String binId2,String binId3, WaveToDownEntity vm){
        Map<String,String> map=new HashMap<>();
        map.put("id", UUIDUtil.getUUID32());
        map.put("createBy", SharedPreferencesUtil.getInstance(mContext).getKeyValue(Constance.SHAREP.LOGINNAME));
        map.put("goodsId",vm.getGoodsId());//商品编码
        map.put("waveId",vm.getWaveId());//原始单据行项目
        map.put("tinId",vm.getTinId());//单位
        map.put("binId",vm.getBinId());//单位
        map.put("proData",vm.getProData());//生产日期
        map.put("firstRq",binId3);//库位编码


         JSONObject jsonObject=new JSONObject(map);
        Map<String,String> params=new HashMap<>();
        params.put("waveToDownstr",jsonObject.toString());//上传实体json
        LoadingUtil.showLoading(mContext);
        HTTPUtils.post(mContext, Constance.getwavetosaveControllercURL(), params, new VolleyListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingUtil.hideLoading();ToastUtil.show(mContext, "未知错误");
            }

            @Override
            public void onResponse(String response) {
                PickingSaveVm vm=GsonUtils.parseJSON(response,PickingSaveVm.class);
                if(vm!=null&&vm.isOk()){
                    mListent.save(position);
                }else if(vm!=null){
                    LoadingUtil.hideLoading();
                    ToastUtil.show(mContext,vm.getErrorMsg());
                }else{
                    ToastUtil.show(mContext, "未知错误");
                }
            }
        });
    }

    public interface PickingDetailListent {
        public void save(int position);
        public void setTinId2(int position, String value);
        public void setBinId2(int position, String value);
    }

    static class ViewHolder {
        @BindView(R.id.tv_zhongWenQch)
        TextView tvZhongWenQch;
        @BindView(R.id.tv_omNoticeId)
        TextView tvOmNoticeId;

        @BindView(R.id.tv_tinId3)
        EditText tvTinId3;
        @BindView(R.id.tv_baseGoodscount)
        TextView tvBaseGoodscount;
        @BindView(R.id.tv_binId)
        TextView tvBinId;
        @BindView(R.id.tv_shpMingCheng)
        TextView tvShpMingCheng;
        @BindView(R.id.tv_tinId)
        TextView tvTinId;
        @BindView(R.id.tv_goodsProData)
        TextView tvGoodsProData;
        @BindView(R.id.tv_shlDanWei)
        TextView tvShlDanWei;
        @BindView(R.id.tv_omBeiZhu)
        TextView tvOmBeiZhu;
        @BindView(R.id.btn_save)
        Button btnSave;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
