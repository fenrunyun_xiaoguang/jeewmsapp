package com.jeewms.www.wms.ui.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jeewms.www.wms.bean.bean.MessageEvent;
import com.jeewms.www.wms.bean.vm.WaveToDownEntity;
import com.jeewms.www.wms.ui.itemview.WavetodownItemView;
import com.jeewms.www.wms.util.LoadingUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by 13799 on 2018/6/7.
 */

public class WavetoDownCardsAdapter extends BaseAdapter{

    Context mContext;
    private List<WaveToDownEntity> mPickingList;

    public List<WaveToDownEntity> getmPickingList() {
        return mPickingList;
    }

    public void setmPickingList(List<WaveToDownEntity> mPickingList) {
        this.mPickingList = mPickingList;
    }

    public WavetoDownCardsAdapter(Context context){
        mContext=context;
    }
    @Override
    public int getCount() {
        return mPickingList==null?0:mPickingList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        WavetodownItemView itemView=null;
        if(view==null){
            itemView=new WavetodownItemView(mContext);
            view=itemView.getView();
            view.setTag(itemView);
        }else{
            itemView= (WavetodownItemView) view.getTag();
        }
        itemView.bindView(mPickingList.get(i),i);
        itemView.setListent(new WavetodownItemView.PickingDetailListent() {
            @Override
            public void save(final int position) {
                LoadingUtil.hideLoading();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        remove(position);
                    }
                },600);
            }

            @Override
            public void setTinId2(int position, String value) {
                mPickingList.get(position).setWaveId(value);
            }

            @Override
            public void setBinId2(int position, String value) {
                mPickingList.get(position).setBinId(value);

            }

        });
        return view;
    }
    public void remove(int position){
        mPickingList.remove(position);
        notifyDataSetChanged();
        EventBus.getDefault().post(new MessageEvent(""));
    }



}
